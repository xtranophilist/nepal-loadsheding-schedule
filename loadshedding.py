from google.appengine.ext import db

class Meta(db.Model):
    count = db.IntegerProperty()
    effective_from = db.StringProperty()

class Schedule(db.Model):
    group = db.StringProperty()
    day = db.StringProperty()
    timing = db.StringProperty()
