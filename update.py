import webapp2
import StringIO
from lxml import html
from google.appengine.ext import db
from google.appengine.api import urlfetch
from loadshedding import Schedule
from loadshedding import Meta

class MainPage(webapp2.RequestHandler):
  def get(self):
      url = 'http://dl.dropbox.com/u/60336235/NLS/NLS.html'
      file = urlfetch.fetch(url)
      h = html.parse(StringIO.StringIO(file.content))
      root = h.getroot()
      meta = Meta()
      meta.effective_from = root[0][0].attrib['effdate']
      meta.count = int(root[0][0].attrib['count'])
      db.delete(db.GqlQuery("SELECT __key__ FROM Meta").fetch(1)) #delete everything from Meta table
      meta.put() #write new data

      #delete everything from Schedule table
      db.delete(db.GqlQuery("SELECT __key__ FROM Schedule").fetch(100))
      for item in root[0][0]:
         schedule = Schedule()
         schedule.group = item[0].text
         schedule.day = item[1].text
         schedule.timing = item[2].text.replace(',','<br />')
         db.put(schedule)
 
      self.response.headers['Content-Type'] = 'text/plain'
      self.response.out.write('Datastore updated from '+url)

app = webapp2.WSGIApplication([('/update', MainPage)],
                              debug=True)


