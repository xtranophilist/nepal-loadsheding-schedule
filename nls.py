import webapp2
import os
import calendar
import datetime
from google.appengine.ext.webapp import template
from google.appengine.ext import db
from loadshedding import Schedule
from loadshedding import Meta

class MainPage(webapp2.RequestHandler):
  def get(self):
    schedules = db.GqlQuery("SELECT * FROM Schedule")
    meta = db.GqlQuery("SELECT * FROM Meta").get()
    entries = {}
    all_items = []

    for schedule in schedules:
	if not schedule.group in entries.keys():
	   entries[schedule.group] = {}
        entries[schedule.group][schedule.day]=schedule.timing

    for x in range(1, int(meta.count)+1):
        all_items.append(entries[str(x)])
    

    template_values = {
                'effective_from': meta.effective_from,
                'entries':all_items,
                'day': calendar.day_name[datetime.datetime.now().weekday()]
        }

    path = os.path.join(os.path.dirname(__file__), 'index.html')
    self.response.out.write(template.render(path, template_values))

app = webapp2.WSGIApplication([('/', MainPage)],
                              debug=True)


